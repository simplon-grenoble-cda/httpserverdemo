package com.company;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;

public class HelloHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        /**
         * This method is executed each time the server receive a request
         * on the endpoint registered in main().
         *
         * You can use the method argument to get data about the
         * request
         *
         * In this example, we don't use data from the request, we
         * always send the same response
         */


        System.out.println("Hey ! I got a request on /hello, I'm going to send a relpy");

        /**
         * I use the JSONObject library to build a JSON response object
         */
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("hello", "world");

        /**
         * In the end, the response must always be a string (here, a string representing
         * a JSON object)
         */
        String responseBody = jsonResponse.toString();

        /**
         * I setup the HTTP response to be sent for the request
         */

        // 1. Set the response HTTP headers to "application/json", because we send
        //    back JSON data, instead of the expected default which would be an HTML document
        httpExchange.getResponseHeaders().add("Content-type", "application/json");


        // 2. Send the response status code. 200 means "OK", this is the case of success.
        //    The lib ask we specify here the size of the response body that will be sent
        httpExchange.sendResponseHeaders(200, responseBody.length());

        // 3. Send the response body (using an OutputStream, the request is sent fully when
        //    once we close the outputstream
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(responseBody.getBytes());
        outputStream.close();
    }

}
